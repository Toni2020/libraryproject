import java.util.ArrayList;

public class Library {
    
    private ArrayList<Item> items;

    public Library() {
        this.items = new ArrayList<Item>();
    }

    public void addItem(String type, String[] details) {

        Item item = new Item();

        switch(type) {
            case "Book":
                item = new Book(details);
                break;

            case "CD":
                item = new CD(details);
                break;

            case "DVD":
                item = new DVD(details);
                break;

            case "Periodical":
                item = new Periodical(details);
                break;

            default:
                System.out.println("Invalid type.");
                return;
        }

        this.items.add(item);
    }

    public void removeItem(String title) {
        for(Item item : items) {
            if(item.getTitle().equalsIgnoreCase(title)) {
                System.out.println("Item found and removed.");
                items.remove(item);
                return;
            }
        }
        System.out.println("Item not found.");        
    }

    public void borrowItem(String title) {
        for(Item item : items) {
            if(item.getTitle().equalsIgnoreCase(title)) {
                if(item.borrowable()) {
                    System.out.println("Item borrowed.");
                    items.remove(item);
                    return;
                } else {
                    System.out.println("Item is not borrowable.");
                    return;
                }
            }
        }
        
    }

    public ArrayList<Item> getItems() {
        return items;
    }
}