public class DVD extends Item {

    public DVD(String[] args) {

        if(args.length == 1) {
            this.setTitle(args[0]);
        } else if (args.length > 1) {
            System.err.println("Too many paramaters!");
            return;
        } else {
            System.err.println("Missing parameters!");
            return;
        }      
        
    }

    public String printDetails() {
        return "\nTitle: " + getTitle();
    }

}