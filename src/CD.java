public class CD extends Item {
    
    private String artist;

    public CD(String[] args) {

        if(args.length == 2) {
            this.artist = args[0];
            this.setTitle(args[1]);
        } else if (args.length > 2) {
            System.err.println("Too many paramaters!");
            return;
        } else {
            System.err.println("Missing parameters!");
            return;
        }      
        
    }

    public String printDetails() {
        return "\nTitle: " + getTitle() + "\nArtist: " + getArtist();
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

}