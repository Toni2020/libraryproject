import java.util.Scanner;

public class Menu {
    
    private Scanner input;
    private Library library;

    public Menu(Scanner input, Library library) {
        this.input = input;
        this.library = library;
    }
    
    public void printOptions() {
        while(true) {
            //Print Options
            System.out.println("\n----------------------");
            System.out.println("----------------------");
            System.out.println("1. Add an item.");
            System.out.println("2. Remove an item.");
            System.out.println("3. Borrow an item.");
            System.out.println("4. List items.");
            System.out.println("5. Exit");
            System.out.println("----------------------");
            System.out.println("----------------------\n");

            //Wait for input
            int userChoice = Integer.valueOf(input.nextLine());

            //Decide what to do based on input
            switch(userChoice) {
                case 1:
                    menuAddItem();
                    break;
                case 2:
                    menuRemoveItem();
                    break;
                case 3:
                    menuBorrowItem();
                    break;
                case 4:
                    menuListItems();
                    break;
                case 5:
                    return;
                default:
                    System.out.println("Invalid choice.");
                    break;
            }
        }
    }

    private void menuAddItem() {

        System.out.println("\nEnter item type. (Book, CD, DVD, Periodical");
        String type = input.nextLine();
        
        switch(type) {
            case "Book":
                menuAddBook();
                break;
            
            case "CD":
                menuAddCD();
                break;

            case "DVD":
                menuAddDVD();
                break;

            case "Periodical":
                menuAddPeriodical();
                break;
            
            default:
                System.err.println("Invalid Type");
                break;
        }
    }

    private void menuAddBook() {
        System.out.println("Title:");
        String title = input.nextLine();

        System.out.println("Author:");
        String author = input.nextLine();

        library.addItem("Book", new String[]{title, author});
    }

    private void menuAddCD() {
        System.out.println("Artist:");
        String artist = input.nextLine();

        System.out.println("Title:");
        String album = input.nextLine();

        library.addItem("CD", new String[]{artist, album});
    }

    private void menuAddDVD() {
        System.out.println("Title:");
        String title = input.nextLine();

        library.addItem("DVD", new String[]{title});
    }

    private void menuAddPeriodical() {
        System.out.println("Title:");
        String title = input.nextLine();

        System.out.println("Date:");
        String date = input.nextLine();

        library.addItem("Periodical", new String[]{title, date});
    }

    private void menuRemoveItem() {
        System.out.println("Enter title to remove: ");
        String title = input.nextLine();

        library.removeItem(title);
    }

    private void menuBorrowItem() {
        System.out.println("Enter title to borrow: ");
        String title = input.nextLine();

        library.borrowItem(title);
    }

    private void menuListItems() {
        for(Item item : library.getItems()) {
            System.out.println(item.printDetails());
        }
    }

}