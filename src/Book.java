public class Book extends Item {

    private String author;

    public Book(String[] args) {

        if(args.length == 2) {
            this.setTitle(args[0]);
            this.author = args[1];
        } else if (args.length > 2) {
            System.err.println("Too many parameters!");
            return;
        } else {
            System.err.println("Missing parameters!");
            return;
        }      
        
    }

    public String printDetails() {
        return "\nTitle: " + getTitle() + "\nAuthor: " + getAuthor();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    
}