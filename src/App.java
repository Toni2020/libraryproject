import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        Library library = new Library();

        Menu menu = new Menu(scanner, library);
        menu.printOptions();

    }
}
