public class Periodical extends Item {

    private String date;

    public Periodical(String[] args) {

        if(args.length == 2) {
            this.setTitle(args[0]);
            this.date = args[1];
        } else if (args.length > 2) {
            System.err.println("Too many paramaters!");
            return;
        } else {
            System.err.println("Missing parameters!");
            return;
        }      
        
    }

    public String printDetails() {
        return "\nTitle: " + getTitle() + "\nDate: " + getDate();
    }

    @Override
    public boolean borrowable() {
        return false;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}